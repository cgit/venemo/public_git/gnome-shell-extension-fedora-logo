Name:           gnome-shell-extension-fedora-logo
Version:        1.0
Release:        2%{?dist}
Summary:        Simple Gnome shell extension to add the Fedora logo to the shell
Group:          User Interface/Desktops
License:        MIT
#Actually this is way too small to have its own URL, but here is the announcement.
URL:            http://lists.fedoraproject.org/pipermail/design-team/2011-April/004215.html
Source0:        http://venemo.fedorapeople.org/sources/%{name}-%{version}.tar.gz
BuildArch:      noarch
Requires:       gnome-shell >= 3.0.0.2, fedora-logos
#This is for those who installed the package with the old name
Obsoletes:      fedora-logo-gnome-shell-extension

%description 
%{name} is a simple Gnome shell extension that adds the Fedora logo
to the Activities button of the Gnome shell.

%prep
%setup -q

%build
%{nil}

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/fedoralogo@fedoraproject.org/
install -pm 644 extension.js $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/fedoralogo@fedoraproject.org/
install -pm 644 metadata.json $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/fedoralogo@fedoraproject.org/

%files
%defattr(-,root,root,-)
%{_datadir}/gnome-shell/extensions/fedoralogo@fedoraproject.org/extension.js
%{_datadir}/gnome-shell/extensions/fedoralogo@fedoraproject.org/metadata.json
%doc LICENSE
%doc README

%changelog
* Sun May 16 2011 Timur Kristóf <venemo@fedoraproject.org> 1.0-2
- Fixed issues in the review request (RHBZ#696357)

* Sun Apr 10 2011 Timur Kristóf <venemo@fedoraproject.org> 1.0-1
- Initial version
